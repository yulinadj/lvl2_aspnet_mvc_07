﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_07.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);

            /* ini kalau pake passing parameter dari vs */
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter ("FirstName", "Ken");
            report.ServerReport.ReportPath = "/ReportNawadata";
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();
            /*ini kalo panggil langsung*/
            //report.ServerReport.ReportPath = "/ReportNawadata";
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}